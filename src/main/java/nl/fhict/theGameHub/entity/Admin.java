package nl.fhict.theGameHub.entity;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("admin")
public class Admin extends User {

    private String secretQuestionAnswer;

    public Admin()
    {

    }

    public Admin(String firstName, String lastName, String username, String password, String secretQuestionAnswer) {
        super(firstName, lastName, username, password);
        this.secretQuestionAnswer = secretQuestionAnswer;
    }

}
