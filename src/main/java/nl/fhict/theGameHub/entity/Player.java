package nl.fhict.theGameHub.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@DiscriminatorValue("player")
public class Player extends User {

    private String emailAddress;

    @OneToMany(mappedBy = "player")
    private List<Leaderboard> results;

    public Player()
    {

    }

    public Player(String firstName, String lastName, String username, String password, String emailAddress, List<Leaderboard> results) {
        super(firstName, lastName, username, password);
        this.emailAddress = emailAddress;
        this.results = results;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public List<Leaderboard> getResults() {
        return results;
    }

    public void setResults(List<Leaderboard> results) {
        this.results = results;
    }

}
