package nl.fhict.theGameHub.entity;

import javax.persistence.*;

@Entity
@Table
public class Leaderboard {


    @Id
    @GeneratedValue
    private int id;
    @ManyToOne
    @JoinColumn(name = "player_id")
    private Player player;
    private Double result;
    private String  game;

    public Leaderboard() {

    }

    public Leaderboard(int id, Player player, Double result, String game) {

        this.id = id;
        this.player = player;
        this.result = result;
        this.game = game;

    }

    public int getId()
    {
        return id;
    }

    public Player getPlayerId() {
        return player;
    }

    public void setPlayerId(Player player) {
        this.player = player;
    }

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }
}

