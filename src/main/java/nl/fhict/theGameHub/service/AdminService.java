package nl.fhict.theGameHub.service;

import nl.fhict.theGameHub.entity.Admin;
import nl.fhict.theGameHub.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {

    @Autowired
    private AdminRepository adminRepository;

    public Admin createAdminAccount(Admin account)
    {
        return adminRepository.save(account);
    }

    public List<Admin> getAllAdmins()
    {
        return adminRepository.findAll();
    }

    public Admin getAdminByUsername(String username)
    {
        return adminRepository.findByUsername(username);
    }

    public String deleteAdminAccount(int id)
    {
        adminRepository.deleteById(id);
        return "Account deleted";
    }

    public Admin updateAdminAccount(Admin account)
    {
        Admin existingAccount = adminRepository.findById(account.getId()).orElse(adminRepository.findByUsername(account.getUsername()));
        existingAccount.setUsername(account.getUsername());
        existingAccount.setFirstName(account.getFirstName());
        existingAccount.setLastName(account.getLastName());
        existingAccount.setPassword(account.getPassword());
        return adminRepository.save(existingAccount);
    }

}
