package nl.fhict.theGameHub.service;

import nl.fhict.theGameHub.entity.Player;
import nl.fhict.theGameHub.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    public Player createPlayerAccount(Player account)
    {
        return playerRepository.save(account);
    }

    public List<Player> getAllPlayers()
    {
        return playerRepository.findAll();
    }

    public Player getPlayerByUsername(String username)
    {
        return playerRepository.findByUsername(username);
    }

    public String deletePlayerAccount(int id)
    {
        playerRepository.deleteById(id);
        return "Account deleted";
    }

    public Player updatePlayerAccount(Player account)
    {
        Player existingAccount = playerRepository.findById(account.getId()).orElse(playerRepository.findByUsername(account.getUsername()));
        existingAccount.setUsername(account.getUsername());
        existingAccount.setFirstName(account.getFirstName());
        existingAccount.setLastName(account.getLastName());
        existingAccount.setPassword(account.getPassword());
        existingAccount.setEmailAddress(existingAccount.getEmailAddress());
        return playerRepository.save(existingAccount);
    }
}
