package nl.fhict.theGameHub.service;

import nl.fhict.theGameHub.entity.Leaderboard;
import nl.fhict.theGameHub.entity.Player;
import nl.fhict.theGameHub.repository.LeaderboardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LeaderboardService {

    @Autowired
    private LeaderboardRepository leaderboardRepository;

    public Leaderboard createLeaderboardResult(Leaderboard result)
    {
        return this.leaderboardRepository.save(result);
    }

    public List<Leaderboard> getAllResults()
    {
        return leaderboardRepository.findAll();
    }

    public Leaderboard getResultByPlayer(Player player)
    {
        return leaderboardRepository.findByPlayer(player);
    }
}
