package nl.fhict.theGameHub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheGameHubApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheGameHubApplication.class, args);
	}

}
