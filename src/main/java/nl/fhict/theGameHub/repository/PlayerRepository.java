package nl.fhict.theGameHub.repository;

import nl.fhict.theGameHub.entity.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player, Integer> {

    Player findByUsername(String username);
}
