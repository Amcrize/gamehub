package nl.fhict.theGameHub.repository;

import nl.fhict.theGameHub.entity.Leaderboard;
import nl.fhict.theGameHub.entity.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LeaderboardRepository extends JpaRepository<Leaderboard, Integer> {

    Leaderboard findByPlayer(Player player);
}
