package nl.fhict.theGameHub.controller;

import nl.fhict.theGameHub.entity.Player;
import nl.fhict.theGameHub.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @PostMapping("/player/{account.getId()}")
    public Player createPlayerAccount(@RequestBody @PathVariable Player account)
    {
        return playerService.createPlayerAccount(account);
    }

    @GetMapping("/players")
    public List<Player> getAllPlayers()
    {
        return playerService.getAllPlayers();
    }

    @GetMapping("/player/{username}")
    public Player getPlayerByUsername(@PathVariable String username)
    {
        return playerService.getPlayerByUsername(username);
    }

    @PutMapping("/player/update/{account.getId()}")
    public Player updatePlayerAccount(@RequestBody @PathVariable Player account)
    {
        return playerService.updatePlayerAccount(account);
    }

    @DeleteMapping("/player/delete/{id}")
    public String deletePlayerAccount(@PathVariable int id)
    {
        return playerService.deletePlayerAccount(id);
    }

}
