package nl.fhict.theGameHub.controller;

import nl.fhict.theGameHub.entity.Admin;
import nl.fhict.theGameHub.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @PostMapping("/admin/{account.getId()}")
    public Admin createPlayerAccount(@RequestBody @PathVariable Admin account)
    {
        return adminService.createAdminAccount(account);
    }

    @GetMapping("/admins")
    public List<Admin> getAllPlayers()
    {
        return adminService.getAllAdmins();
    }

    @GetMapping("/admins/{username}")
    public Admin getAdminsByUsername(@PathVariable String username)
    {
        return adminService.getAdminByUsername(username);
    }

    @PutMapping("/admin/update/{account.getId()}")
    public Admin updatePlayerAccount(@RequestBody @PathVariable Admin account)
    {
        return adminService.updateAdminAccount(account);
    }

    @DeleteMapping("/admin/delete/{id}")
    public String deletePlayerAccount(@PathVariable int id)
    {
        return adminService.deleteAdminAccount(id);
    }

}
