package nl.fhict.theGameHub.controller;

import nl.fhict.theGameHub.entity.Leaderboard;
import nl.fhict.theGameHub.entity.Player;
import nl.fhict.theGameHub.service.LeaderboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class LeaderboardController {

    @Autowired
    private LeaderboardService leaderboardService;

    @PostMapping("/leaderboard/{result.getId()}")
    public Leaderboard createNewResult(@RequestBody @PathVariable Leaderboard result)
    {
        return leaderboardService.createLeaderboardResult(result);
    }

    @GetMapping("/leaderboard")
    public List<Leaderboard> getLeaderboard()
    {
        return leaderboardService.getAllResults();
    }

    @GetMapping("/leaderboard/{player.getUsername()}")
    public Leaderboard getResultByUsername(@PathVariable Player player)
    {
        return leaderboardService.getResultByPlayer(player);
    }

}
