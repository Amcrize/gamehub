package service;

import nl.fhict.theGameHub.entity.Admin;
import nl.fhict.theGameHub.repository.AdminRepository;
import nl.fhict.theGameHub.service.AdminService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AdminService.class)
public class AdminServiceTest {

    @Autowired
    private AdminService adminService;

    @MockBean
    private AdminRepository adminRepository;

    @Test
    public void when_createAdminAccount_then_returnAccountCreated()
    {
        int id = 0;
        Admin account = new Admin("Tudor", "Rusu", "trusu", "blabla", "Elena");
        Admin accountSaved = new Admin("Tudor", "Rusu", "trusu", "blabla", "Elena"); 
        setAccountId(accountSaved, id);
        when(adminRepository.save(account)).thenReturn(accountSaved);

        Admin created = adminService.createAdminAccount(account);

        verify(adminRepository).save(account);
        assertNotNull(created.getId());
        assertEquals(id, created.getId());
    }

    private Admin setAccountId(Admin account, int id){

        Field idField;
        try{
            idField = account.getClass().getDeclaredField("id");
            idField.setAccessible(true);
            idField.set(account, id);
        }
        catch(Exception ex){

        }

        return account;
    }

}
