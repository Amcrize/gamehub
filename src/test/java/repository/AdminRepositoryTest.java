package repository;

import nl.fhict.theGameHub.entity.Admin;
import nl.fhict.theGameHub.repository.AdminRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = AdminRepository.class)
public class AdminRepositoryTest {

    @MockBean
    private EntityManager entityManager;

    @MockBean
    private AdminRepository adminRepository;

    @Test
    public void whenFindByUsername_thenReturnAdmin()
    {
        Admin tudor = new Admin("Tudor", "Rusu", "trusu", "blabla", "Elena");
        entityManager.persist(tudor);
        entityManager.flush();
        when(adminRepository.findByUsername(tudor.getUsername())).thenReturn(tudor);
        Admin found = adminRepository.findByUsername(tudor.getUsername());
        assertThat(found.getUsername()).isEqualTo(tudor.getUsername());
    }

}
